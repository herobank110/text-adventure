"""Standalone player for text based adventures made using
the editor utility"""


# imports

# for GUI
from tkinter import *
from tkinter import simpledialog
from TkinterGenericForm import *

# import core functionality for text adventure
from text_adventure_base import *

# for sounds
import winsound


# globals

datasuffix = "_data.txt" # suffix for data files for an adventure

filename = "start_screen.txt" # initial text adventure
savefilename = filename[:-4] + datasuffix # initial data file

def init():
    global currentPageID
    createGameWidgets()
    currentPageID = "0" # start at page 0 by default
    window.title(filename.split("/")[-1][:-4])
    loadUIFromFile(filename, currentPageID)

def loadUIFromFile(filename, ID):
    global currentPageID
    parts = loadFromFile(filename, ID)
    title = parts[1].replace("/n", "\n").replace("``",",")
    fields = [p for i, p in enumerate(parts) if i>1 and parts[i-1]=="<Choice>"]
    currentPageID = ID
    createGameWidgets(title, fields)

def onTextSelectConfirm(Idx):
    global currentPageID
    if Idx <= -1:
        return

    # perform actions based on meta tags for this choice
    metaTags = getMetaTags(filename,currentPageID, Idx)

    for tag in metaTags:
        if tag != "":

            brokenTag = breakMetaTag(tag)
            if len(brokenTag) >= 2:
                command = brokenTag[0]
                data = formatTagsNew(brokenTag[1])

                if command == "command":
                    exec(data)
                elif command == "data":
                    dataParts = data.split(" ")
                    dataKey = dataParts[0]
                    dataVal = dataParts[1].replace("_", " ")
                    try:
                        dataVal = eval(dataVal)
                    except:
                        pass
                    writeDataToFile(savefilename, {dataKey:dataVal})
                elif command == "note":
                    print(data)
                elif command == "conditional":
                    dataParts = data.split(" ")
                    condition = dataParts[0]
                    condition_true = dataParts[1]
                    condition_false = dataParts[2]
                elif command == "input":
                    dataParts = data.split(" ")
                    dataPrompt = dataParts[0]
                    dataKey = dataParts[1].replace("_", " ")
                    inputValue = simpledialog.askstring("Input", dataPrompt)
                    # sanitize input
                    if inputValue:
                        inputValue = inputValue.replace(" ", "_").replace(",", "``")
                    else:
                        inputValue = ""
                    writeDataToFile(savefilename, {dataKey:inputValue})
                elif command == "load_adv":
                    dataKey = data.split(" ")[0]
                    setAdventureFilesFromKey(dataKey)
                    return init()


    # Determine next pageID ...
    newchoices = loadFromFile(filename, currentPageID)[2:] # skip page id and title

    if len(newchoices) > 0: # .. using selected choice
        leadsToList = [it for i, it in enumerate(newchoices) if i>1 and newchoices[i-2]=="<Choice>"]
        leadsTo = leadsToList[Idx]

        if "condition" in locals() and condition: # .. using condition
            try:
                leadsTo = condition_true if eval(condition) else condition_false
            except:
                pass
    else:
        try: # .. using next pageID
            leadsTo = str(int(currentPageID) + 1)
        except ValueError: # .. using same pageID @TODO allow setting leads to with no choices
            leadsTo = currentPageID

    # Go to next page
    loadUIFromFile(filename, leadsTo)
    writeDataToFile(savefilename, {"pageReached":leadsTo})

def setAdventureFilesFromKey(key):
    global filename, savefilename
    savedData = loadAllDataFromFile(savefilename)
    if key in savedData:
        adv_id = savedData[key]
        filename = adventuresdir + "/" + adv_id + "/" + adv_id + ".txt"
        savefilename = filename[:-4] + datasuffix

def createGameWidgets(title="title", fields=[]):
    global myTextSelect, label_image

    # destroy existing page's widgets
    if "myTextSelect" in globals() and myTextSelect.winfo_exists():
        myTextSelect.destroy()
    if "label_image" in globals() and label_image.winfo_exists():
        label_image.destroy()
    
    # add page image, if any
    try:
        photoImage = getImageForPage(filename, currentPageID)
        if photoImage:
            label_image = Label(window, image=photoImage)
            label_image.image = photoImage
            label_image.pack(padx=10, pady=10)
    except:
        pass

    # format text in title and fields
    title = formatTagsNew(title)
    for i, field in enumerate(fields):
        fields[i] = formatTagsNew(field)

    # create text selection, with title and fields
    myTextSelect = TextSelect(window, title=title, fields=fields, onConfirm=onTextSelectConfirm)
    myTextSelect.pack(padx=20, pady=20)

    # play music, if any
    if "currentPageID" in globals():
        playMusicForPage(filename, currentPageID)

def formatTagsNew(inText):
    """Same as formatTagsWithData, but supports both formats of {key} and <data>(key)"""
    return formatTagsWithData(formatTagsWithData(inText), "{", "}")

def formatTagsWithData(inText, identifier="<data>", endData=")"):
    """Format inText by replacing all <data>(key) with saved values, if possible"""
    newText = inText

    while identifier in inText:
        start = inText.rfind(identifier)
        end = start + 1 + inText[start:].find(endData)

        tag = inText[start:end]
        try: # use traditional <data>(key) method
            key = breakMetaTag(tag)[1]
        except IndexError: # otherwise support new {key} method
            key = tag.replace(identifier, "").replace(endData, "")

        savedData = loadAllDataFromFile(savefilename)
        inText = inText.replace(tag, "") # remove from testing text
        if key in savedData:
            newText = newText.replace(tag, savedData[key]) # replace with value in new text

    return newText


def main():
    global window
    window = Tk()
    init()
    window.mainloop()

if __name__ == '__main__':
    main()

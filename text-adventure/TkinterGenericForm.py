from tkinter import *

class Form(Frame):

    def config(self, kwargs={}, **kw):
        if "onFocus" in kwargs:
            self.onFocusCallback = kwargs["onFocus"]

        # Set the index of entries to hide characters for (i.e. for passwords)
        if "hideIndex" in kwargs:
            self.hideIndex = kwargs ["hideIndex"]

        if "labelsSticky" in kwargs:
            self.labelsSticky = kwargs["labelsSticky"]

        if ("advancedTitle" in kwargs):
            self.advancedTitle = kwargs["advancedTitle"]

        if ("titleSize" in kwargs):
            self.titleSize = kwargs["titleSize"]

        if ("fg" in kwargs):
            self.fg = kwargs["fg"]


    def __init__(self, *args, **kwargs):

        # Check for some keywords before initialising the frame
        self.bg = "SystemButtonFace"
        if ("bg" in kwargs):
            self.bg = kwargs["bg"]


        Frame.__init__(self, *args, bg=self.bg) #initialise frame object without additional data

        # Set default variables for Form
        self.onFocusCallback = None     # Function to call when an entry box receives focus
        self.hideIndex = -1             # Index of fields to hide (i.e. for passwords)
        self.entries = {}               # Dictionary of entry widget reference for each field
        self.clearButtons = {}          # Dictionary of buttons to clear text in entry widget for each field
        self.labelsSticky = "e"         # Side to stick to, applied to labels for fields
        self.advancedFields = None      # Optional fields that have their own section which can be collapsed/opened. MUST HAVE DIFFERENT NAMES TO FIELDS
        self.advancedTitle = None       # Optional title for advanced section (if advanced fields are included too)
        self.titleSize = 14             # Font size of title
        self.fg = "SystemButtonText"    # Color of text in entries and buttons

        #button alt code text
        self.downArrow = "▼" # alt 31
        self.upArrow = "▲" # alt 30
        self.clearText = "X" # alt 11199

        # Set additional options by calling config
        self.config(kwargs)

         # Create title label
        if "title" in kwargs and kwargs["title"] != None:
            Label(self, text=kwargs["title"], font="arial " + str(self.titleSize), bg=self.bg, fg=self.fg).pack(side="top", anchor=N, pady=3)

        # Create required widgets for each field
        if ("fields" in kwargs):
            self.fieldsFrame = Frame(self, bg=self.bg)
            # Create main fields

            # Create form with only entry widgets
            if (not isinstance(kwargs["fields"], dict)):
                for field in kwargs["fields"]:
                    i = kwargs["fields"].index(field)
                    # Create label with field name
                    Label(self.fieldsFrame, text=field, bg=self.bg, fg=self.fg).grid(row=i, column=0, padx=5, sticky=self.labelsSticky)
                    # Create entry for field, add it to dictionary
                    self.entries[field] = Entry(self.fieldsFrame)
                    self.entries[field].bind("<FocusIn>", self.showEntryClearButton)
                    self.entries[field].grid(row=i, column=1, pady=3, padx=0)
                    # Hide text in this entry if necessary
                    if (i == self.hideIndex):
                        self.entries[field].config(show="*")
                    # Add a button to clear text in the entry # alt 11199
                    self.clearButtons[field] = Button(self.fieldsFrame, text=self.clearText, fg=self.fg, bg=self.bg, relief="flat", state="disabled", disabledforeground=self.fieldsFrame.cget("bg"), activebackground=self.bg, activeforeground=self.fg, takefocus=0)
                    self.clearButtons[field].bind("<Button-1>", self.clearTextInEntry)
                    self.clearButtons[field].grid(row=i, column=2, pady=3)

            # Create form with each field specified mode of entry
            elif (isinstance(kwargs["fields"], dict)):
                for field in kwargs["fields"]:
                    i = kwargs["fields"].index(field)
                    # Create label with field name
                    Label(self.fieldsFrame, text=field).grid(row=i, column=0, padx=5, sticky=self.labelsSticky)
                    # Create specified widget for field, add it to dictionary
                    # Currently only entry,
                    self.entries[field] = Entry(self.fieldsFrame)
                    self.entries[field].bind("<FocusIn>", self.showEntryClearButton)
                    self.entries[field].grid(row=i, column=1, pady=3, padx=0)
                    # Hide text in this entry if necessary
                    if (i == self.hideIndex):
                        self.entries[field].config(show="*")
                    # Add a button to clear text in the entry # alt 11199
                    self.clearButtons[field] = Button(self.fieldsFrame, text=self.clearText, relief="flat", state="disabled", disabledforeground=self.fieldsFrame.cget("bg"), takefocus=0)
                    self.clearButtons[field].bind("<Button-1>", self.clearTextInEntry)
                    self.clearButtons[field].grid(row=i, column=2, pady=3)

            self.fieldsFrame.pack()

            # Add hidden advanced options if necessary
            if ("advancedFields" in kwargs):

                # Create button to collapse/open advanced menu
                self.advancedButton = Button(self.fieldsFrame, text=self.downArrow, cursor="hand2", relief="flat", fg=self.fg, bg=self.bg, activebackground=self.bg, activeforeground=self.fg) #alt 31
                self.advancedButton.bind("<Return>", self.onClickAdvanced)
                self.advancedButton.bind("<Button-1>", self.onClickAdvanced)
                self.advancedButton.grid()

                # Create another form inside this one containing advanced fields
                self.advFrame = Frame(self, bg=self.bg)
                self.advancedForm = Form(self.advFrame, title=self.advancedTitle, titleSize=11, fields=kwargs["advancedFields"], fg=self.fg, bg=self.bg)
                self.advancedForm.pack()

                # Update dictionaries to include fields in advanced form
                self.entries.update(self.advancedForm.entries)
                self.clearButtons.update(self.advancedForm.clearButtons)
                self.advancedForm.clearButtons = self.clearButtons

    def onClickAdvanced(self, event):
        # Alternate between up and down pointing triangles
        if (event.widget.cget("text") == self.downArrow): # alt 31
            event.widget.config(text = self.upArrow) #alt 30
            self.advFrame.pack() # Show adv. frame
        else:
            event.widget.config(text=self.downArrow)
            self.advFrame.pack_forget() # Hide adv. frame

    # Return a dictionary with each field and its current entered data
    def getAllEntries(self):
        enteredData = {}
        for entry in self.entries:
            enteredData[entry] = self.entries[entry].get()
        return enteredData

    # Attempt to show the clear button for a field's entry widget, and hide all others
    def showEntryClearButton(self, event=None):
        # Set all clear buttons to be disabled
        for button in self.clearButtons:
            self.clearButtons[button].config(state="disabled")
        # Find the field for the entry which just gained focus
        for entry in self.entries:
            if(self.entries[entry] == event.widget):
                # Show only the clear button for the found field
                self.clearButtons[entry].config(state="normal")
                # Call the bound function (if any)
                if (self.onFocusCallback != None):
                    self.onFocusCallback

    # Clear text in the field's entry when the clear button for that field is pressed
    def clearTextInEntry(self, event):
        # Don't do anything if the clicked button is disabled
        if (event.widget.cget("state") == "disabled"):
            return
        # Find the field for the button which was pressed
        for button in self.clearButtons:
            if (event.widget == self.clearButtons[button]):
                self.entries[button].delete(0, "end")



class TextSelect(Frame):

    def config(self, kwargs={}, **kw):
        for kw in kwargs:
            if kw == "onFocus":
                self.onFocusCallback = kwargs["onFocus"]

            if kw == "labelsSticky":
                self.labelsSticky = kwargs["labelsSticky"]

            if kw == "titleSize":
                self.titleSize = kwargs["titleSize"]

            if kw == "fg":
                self.fg = kwargs["fg"]

            if kw == "confirmText":
                self.confirmText = kwargs["confirmText"]

            if kw == "onConfirm":
                self.onConfirmCallback = kwargs["onConfirm"]

            if kw == "createConfirm":
                self.createConfirm = kw["createConfirm"]

    def __init__(self, *args, **kwargs):

        # Check for some keywords before initialising the frame
        self.bg = "SystemButtonFace"
        if ("bg" in kwargs):
            self.bg = kwargs["bg"]

        Frame.__init__(self, *args, bg=self.bg) #initialise frame object without additional data

        # Set default variables for Form
        self.onFocusCallback = None     # Function to call when an rbn box receives focus
        self.onConfirmCallback = None   # Function to call after confirm is pressed
        self.rbns = {}                  # Dictionary of rbn widget reference for each field
        self.labs = {}                  # Dictionary of lab widget reference for each field
        self.labelsSticky = "w"         # Side to stick to, applied to labels for fields
        self.titleSize = 14             # Font size of title
        self.fg = "SystemButtonText"    # Color of text in rbns and buttons
        self.confirmText = "Confirm"    # Text to display in the confirm button
        self.textX = 20                 # Width of text widget in letters
        self.textY = 4                  # Height of text widget in lines of text
        self.selRbnVar = IntVar()       # Index of selected radio button
        self.selRbnVar.set(-1)
        self.createConfirm = True       # Whether to make a Confirm button or not

        # Set additional options by calling config
        self.config(kwargs)

         # Create title label
        if "title" in kwargs and kwargs["title"] != None:
            self.mainText = Text(self, wrap=WORD, font="arial " + str(self.titleSize), bg=self.bg, fg=self.fg, width=self.textX, height=self.textY, relief=FLAT)
            self.mainText.insert(END, kwargs["title"])
            self.mainText.config(state=DISABLED)
            self.mainText.pack()

        # Create required widgets for each field
        if ("fields" in kwargs):
            self.fieldsFrame = Frame(self, bg=self.bg)
            # Create main fields
            i = 0
            # Create form with rbn widgets
            for field in kwargs["fields"]:
                i = kwargs["fields"].index(field)
                # Create rbn for field, add it to dictionary
                self.rbns[field] = Radiobutton(self.fieldsFrame, variable=self.selRbnVar, value=i, bg=self.bg,activebackground=self.bg)
                self.rbns[field].grid(row=i, column=0, pady=3, padx=0)
                # Create label with field name
                newLab = Label(self.fieldsFrame, text=field, bg=self.bg, fg=self.fg)
                newLab.bind("<Button-1>", self.selFromLab)
                newLab.grid(row=i, column=1, padx=3, sticky=self.labelsSticky)
                self.labs[newLab] = field
            # Create button to confirm selected, if required
            if self.createConfirm:
                Button(self.fieldsFrame, text=self.confirmText, command=self.onUIConfirm,bg=self.bg, fg=self.fg, activebackground=self.bg, activeforeground=self.fg, relief=RIDGE).grid(row=i+1,column=1, sticky=NSEW)
            self.fieldsFrame.pack(anchor=W, padx=3, pady=5)

    def selFromLab(self, e):
        self.rbns[self.labs[e.widget]].select()

    # return index of the selected rbn
    def getSelIdx(self):
        return self.selRbnVar.get()

    def onUIConfirm(self):
        if self.onConfirmCallback == None: return
        selIdx = self.getSelIdx()
        self.onConfirmCallback(selIdx)

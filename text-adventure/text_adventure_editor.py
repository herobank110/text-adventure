"""GUI toolkit to allow for the construction,
editing and previewing of text based adventures"""

# imports

import os
from os import path

# core text adventure functionality
from text_adventure_base import *

# tkinter for GUI
from tkinter import *
from tkinter.ttk import *
from tkinter import messagebox
from tkinter import simpledialog
from tkinter import filedialog
from TkinterGenericForm import *

filename = "" # you can set default starting filename here if you want
maxNumChoices = 4 # maximum number of choices in one question

newDataTypes = {"Save data":"<data>({key} {value})",
                "User input":"<input>({prompt} {key})",
                "Command":"<command>({name})",
                "Note":"<note>({note})",
                "Conditional":"<conditional>({condition} {condition_true} {condition_false})",
                "Load Adventure":"<load_adv>({key})"}

def bindGlobalHotkeys(widget, *other_widgets):
    
    # bind Ctrl-Alt-p to Preview Page
    widget.bind("<Control-Alt-p>", lambda e: onUIPreview())

    # bind Ctrl-s to Save
    widget.bind("<Control-s>", lambda e: onUISave())

    # bind Ctrl-o to load
    widget.bind("<Control-o>", lambda e: onUILoad())
    # bind Ctrl-l to load
    widget.bind("<Control-l>", lambda e: onUILoad())

    # bind Ctrl-Shift-X to Clear
    widget.bind("<Control-Shift-X>", lambda e: onUIClear())    
    

    # bind hotkeys for all extra specified widgets
    for w in other_widgets:
        bindGlobalHotkeys(w)

def createEntryWidgets():
    global frame_preview, previewPage, frame_entry, spinbox_pageID, text_title, frame_addedChoices, spinbox_loadID, entry_textadID, spinbox_imageID, spinbox_musicID

    # preview page - fill be filled in with entry

    if "frame_preview" in globals() and frame_preview.winfo_exists(): frame_preview.destroy()
    frame_preview = LabelFrame(window, text="Page Preview")

    try:
        photoImage = getImageForPage(filename, spinbox_loadID.get())
        if photoImage:
            label_image = Label(frame_preview, image=photoImage)
            label_image.image = photoImage
            label_image.pack(padx=10, pady=10)
    except:
        pass

    previewPage = TextSelect(frame_preview, fields=[], title="")

    # entry for preview page

    if "frame_entry" in globals() and frame_entry.winfo_exists(): frame_entry.destroy()
    frame_entry = LabelFrame(window, text="Page Editor")

    frame_textadID = LabelFrame(frame_entry, text="Project")
    label_textadID = Label(frame_textadID, text="Text Adv ID")
    entry_textadID = Entry(frame_textadID)
    entry_textadID.insert(END, filename.split("/")[-1].replace(".txt", ""))
    entry_textadID.bind("<Return>", saveFilename)

    frame_pageID = Frame(frame_entry)
    label_pageID = Label(frame_pageID, text="Page ID")
    spinbox_pageID = Spinbox(frame_pageID, from_=0, to=1000)

    frame_image = Frame(frame_entry)
    label_imageID = Label(frame_image, text="Image ID Override")
    spinbox_imageID = Spinbox(frame_image, from_=0, to=1000)
    spinbox_imageID.delete(0, END)
    spinbox_imageID.insert(END, "NO OVERRIDE") 

    frame_music = Frame(frame_entry)
    label_musicID = Label(frame_music, text="Music ID Override")
    spinbox_musicID = Spinbox(frame_music, from_=0, to=1000)
    spinbox_musicID.delete(0, END)
    spinbox_musicID.insert(END, "NO OVERRIDE")

    text_title = Text(frame_entry, wrap=WORD, width=20, height=4)

    frame_addedChoices = LabelFrame(frame_entry, text="Choices")
    button_addChoice = Button(frame_addedChoices, text="+", command=onUIAddChoice)

    frame_commands = LabelFrame(frame_entry, text="Save")
    button_clear = Button(frame_commands, text="Clear", command=onUIClear)
    button_preview = Button(frame_commands, text="Preview", command=onUIPreview)
    button_save = Button(frame_commands, text="Save", command=onUISave)

    frame_load = LabelFrame(frame_entry, text="Load")
    label_loadID = Label(frame_load, text="Load ID")
    spinbox_loadID = Spinbox(frame_load, from_=0, to=1000)
    button_load = Button(frame_load, text="Load", command=onUILoad)

    # pack from left
    for w in [frame_preview, frame_entry, label_textadID, entry_textadID, label_pageID, spinbox_pageID, label_imageID, spinbox_imageID, label_musicID, spinbox_musicID, button_clear, button_preview, button_save, label_loadID, spinbox_loadID, button_load]:
        w.pack(side=LEFT, padx=10, pady=5, fill=BOTH, expand=TRUE)

    # pack from top
    for w in [frame_textadID, frame_pageID, frame_image, frame_music, text_title, frame_addedChoices, frame_commands, frame_load]:
        w.pack(padx=10, pady=5, fill=X, expand=TRUE)

    button_addChoice.pack(side=LEFT, anchor=NW, expand=FALSE, padx=5, pady=5)
    previewPage.pack(padx=10, pady=10)

# clear all entered data
def init():
    global choices
    createEntryWidgets()
    choices = []
    #loadUIFromFile(filename, "0")

def writeToFile(filename, line):
    # find line number this pageID is already saved at if possible
    lineNum = -1
    if path.exists(filename):
        with open(filename, "r") as file:
            lines = file.readlines()
            for i, testline in enumerate(lines):
                if testline.split(",")[0] == line.split(",")[0]:
                    lineNum = i
                    break

    # write or update page to file
    if lineNum != -1: # The page was found already in the file (by pageID)
        with open(filename, "r") as file:
            newContents = file.readlines()
            newContents[lineNum] = line
        with open(filename, "w") as file:
            file.writelines(newContents) # rewrite the entire file with altered contents
    else: # The player wasn't found in the file (by name)
        with open(filename, "a") as file:
            file.write(line)

def writeUIToFile(filename):
    global choices, text_title

    # make save line for page
    line = ""
    line += spinbox_pageID.get().replace("," ,"``") + ","
    line += text_title.get(0.0, END).rstrip("\n").replace("\n", "/n").replace("," ,"``") #replace newline with /n so it fits on one line
    # specify image ID if overridden
    if spinbox_imageID.get() and spinbox_imageID.get() != "NO OVERRIDE":
        line += ",<image>,%s,"%spinbox_imageID.get()
    # specify music ID if overridden
    if spinbox_musicID.get() and spinbox_musicID.get() != "NO OVERRIDE":
        line += ",<music>,%s,"%spinbox_musicID.get()


    for c in choices:
        line += ",<Choice>,"
        line += c[2].get().replace("," ,"``") + ","
        line += c[3].get().replace("," ,"``") + ","
        line += "<Meta>,"
        line += c[5].rstrip("\n").replace("/n", ",")

    line = line.replace("\n", "/n") # make sure this will only be one line!!!
    line += "\n" # add newline at end

    writeToFile(filename, line)

def loadUIFromFile(filename, ID):
    global choices
    saveFilename()
    if not path.exists(filename):
        return
    parts = loadFromFile(filename, ID)

    if parts[0] == ID: #is this the line we want?
        init() # clear existing data
        spinbox_pageID.delete(0, END)
        spinbox_pageID.insert(END, ID)
        text_title.insert(END, parts[1].replace("/n", "\n").replace("``", ","))
        # check if image is specified with override
        try:
            img_idx = parts.index("<image>")
            img_text = parts[img_idx+1]
        except ValueError:
            img_text = "NO OVERRIDE"
        spinbox_imageID.delete(0, END)
        spinbox_imageID.insert(END, img_text)
        # check if music is specified with override
        try:
            mus_idx = parts.index("<music>")
            mus_text = parts[mus_idx+1]
        except ValueError:
            mus_text = "NO OVERRIDE"
        spinbox_musicID.delete(0, END)
        spinbox_musicID.insert(END, mus_text)
        # check after the id and title for choices
        i = 0
        readingMeta = False
        for part in parts[2:]:
            if part == "<Choice>":
                onUIAddChoice() # create new widgets and data
                i = 1
                readingMeta = False
            elif part == "<Meta>":
                readingMeta = True
            elif readingMeta:
                # add this meta tag to the meta tags string
                #if part != "":
                c = choices[len(choices)-1]
                choices[len(choices)-1] = (c[0], c[1], c[2], c[3], c[4], c[5]+part+",")
            elif i == 1:
                # set the text in the "Display" entry
                choices[len(choices)-1][2].delete(0,END)
                choices[len(choices)-1][2].insert(END, part)
                i = 2
            elif i == 2:
                # set the text in the "Leads to" spinbox
                choices[len(choices)-1][3].delete(0,END)
                choices[len(choices)-1][3].insert(END, part)
                i = 0
        spinbox_loadID.delete(0,END)
        spinbox_loadID.insert(END, ID)
        onUIPreview()

def saveFilename(e=None):
    global entry_textadID, filename


    adv_id = entry_textadID.get()

    # check if file is already stored in base dir
    if path.exists(adv_id + ".txt"):
        filename = adv_id + ".txt"
    # otherwise store file in correct adventures dir
    else:    
        # check adventures directory exists
        if not path.exists(adventuresdir + "/" + adv_id):
            os.makedirs(adventuresdir + "/" + adv_id)
            os.makedirs(adventuresdir + "/" + adv_id + "/" + adv_id+imagedirsuffix)
            os.makedirs(adventuresdir + "/" + adv_id + "/" + adv_id+musicdirsuffix)
        filename = adventuresdir + "/" + adv_id + "/" + adv_id + ".txt"
    return filename

def onUIClear():
    init()

def onUISave():
    if saveFilename() == ".txt":
        messagebox.showerror("Error", "Please enter a text adventure ID")
        return
    writeUIToFile(filename);
    messagebox.showinfo("Info","Successfully saved to file!")

    # run a preview to get up-to-date preview
    onUIPreview()

def onUILoad():
    saveFilename()
    loadUIFromFile(filename, spinbox_loadID.get())

def onUIPreview():
    global previewPage, choices
    title = text_title.get(0.0, END)
    fields = [c[2].get() for c in choices]
    if "previewPage" in globals() and previewPage.winfo_exists():
        previewPage.destroy()
    previewPage = TextSelect(frame_preview, title=title, fields=fields)
    previewPage.pack(padx=10, pady=10)

def onUIRemoveChoice(event):
    global choices
    for choice in choices:
        btnRem = choice[1]
        if btnRem == event.widget:
            frame_Choice = choice[0]
            frame_Choice.destroy()
            choices.remove(choice)

def onUIChoiceUp(frame):
    global choices
    i = -1
    for i, choice in enumerate(choices):
        if choice[0] == frame:
            break
    if i > 0:
        # swap this choice and the choice one index below
        tempChoice = choices[i-1]
        choices[i-1] = choice
        choices[i] = tempChoice

        # reload widgets in new order
        for c in choices:
            c[0].pack(fill=X, expand=TRUE)
        onUIPreview()

def onUIChoiceDown(frame):
    global choices
    i = len(choices) + 1
    for i, choice in enumerate(choices):
        if choice[0] == frame:
            break
    if i + 1 < len(choices):
        # swap this choice and the choice one index below
        tempChoice = choices[i+1]
        choices[i+1] = choice
        choices[i] = tempChoice

        # reload widgets in new order
        for c in choices:
            c[0].pack_forget
        for c in choices:
            c[0].pack(fill=X, expand=TRUE)
        onUIPreview()

def onUIClickedExtra(event):
    global choices
    for choice in choices:
        btnExt = choice[4]
        if btnExt == event.widget:
            createExtraConfig(choice)

def onUIAddChoice():
    global frame_addedChoices, choices
    if len(choices) >= maxNumChoices:
        return
    frame_newChoice = Frame(frame_addedChoices, pady=10)

    frame_name = Frame(frame_newChoice)
    label_choiceName = Label(frame_name, text="Display", anchor=W)
    entry_name = Entry(frame_name)
    button_remove = Button(frame_name, text="X", takefocus=0)
    button_remove.bind("<Button-1>", onUIRemoveChoice)

    frame_leadsTo = Frame(frame_newChoice)
    label_choiceLeadsTo = Label(frame_leadsTo, text="Leads to", anchor=W)
    spinbox_choiceLeadsTo = Spinbox(frame_leadsTo, from_=0, to=1000)
    button_moveUp = Button(frame_leadsTo, text=r"↑", takefocus=0) #alt 24

    frame_extra = Frame(frame_newChoice)
    label_extra = Label(frame_extra, text="Extra", anchor=W)
    button_extracnf = Button(frame_extra, text="Customise...")
    button_extracnf.bind("<ButtonRelease-1>", onUIClickedExtra)
    button_moveDown = Button(frame_extra, text=r"↓", takefocus=0) #alt 25

    # bind hotkeys to entry widgets
    bindGlobalHotkeys(entry_name, spinbox_choiceLeadsTo)

    # pack from right
    for w in [button_remove, button_moveUp, button_moveDown]:
        w.pack(side=RIGHT, padx=3)

    #pack from left
    for w in [label_choiceName, label_choiceLeadsTo, label_extra]:
        w.pack(side=LEFT, fill=X, expand=TRUE, padx=5)

    # pack from right
    for w in [entry_name, spinbox_choiceLeadsTo, button_extracnf]:
        w.pack(side=RIGHT, fill=X, expand=TRUE, padx=5, pady=2)

    # pack from top
    for w in [frame_newChoice, frame_name, frame_leadsTo, frame_extra]:
        w.pack(fill=X, expand=TRUE)

    # make choices tuple!!!
    choice = (frame_newChoice, button_remove, entry_name, spinbox_choiceLeadsTo, button_extracnf, "")
    choices.append(choice)

    button_moveUp.bind("<ButtonRelease-1>", lambda e: onUIChoiceUp(frame_newChoice))
    button_moveDown.bind("<ButtonRelease-1>", lambda e: onUIChoiceDown(frame_newChoice))

def onUINewDataType(event=None):
    global chosenOption, text_display
    if not "chosenOption" in globals():
        return
    value = chosenOption.get()
    try:
        newDataTemp = newDataTypes[chosenOption.get()] + "\n"
        newData = ""
    except KeyError:
        return

    # input data from user
    if value == "Save data":
        answer_key = simpledialog.askstring("Input", "What key is the data saved at?", parent=tl_extCnf)
        if answer_key:
            answer_value = simpledialog.askstring("Input", "What value will the data have?", parent=tl_extCnf)
            if answer_value:
                newData = newDataTemp.format(key=answer_key, value=answer_value)
    elif value == "User input":
        answer_prompt = simpledialog.askstring("Input", "What text will the user be prompted with (no spaces)?", parent=tl_extCnf)
        if answer_prompt:
            answer_key = simpledialog.askstring("Input", "What key will the input be saved at?", parent=tl_extCnf)
            if answer_key:
                newData = newDataTemp.format(prompt=answer_prompt, key=answer_key)
    elif value == "Conditional":
        answer_condition = simpledialog.askstring("Input", "What is the condition?", parent=tl_extCnf)
        if answer_condition:
            answer_condition_true = simpledialog.askstring("Input", "Page ID when condition is True", parent=tl_extCnf)
            if answer_condition_true:
                answer_condition_false = simpledialog.askstring("Input", "Page ID when condition is False", parent=tl_extCnf)
                if answer_condition_false:
                    newData = newDataTemp.format(condition=answer_condition, condition_true=answer_condition_true, condition_false=answer_condition_false)
    elif value == "Command":
        answer_command = simpledialog.askstring("Input", "What is the command name?", parent=tl_extCnf)
        if answer_command:
            newData = newDataTemp.format(name=answer_command)
    elif value == "Note":
        answer_note = simpledialog.askstring("Input", "What is the note?", parent=tl_extCnf)
        if answer_note:
            newData = newDataTemp.format(note=answer_note)
    elif value == "Load Adventure":
        answer_key = simpledialog.askstring("Input", "What key is the adventure filename saved at?")
        if answer_key:
            newData = newDataTemp.format(key=answer_key)

    # insert data to text display
    curtxt = text_display.get(0.0, END)
    newtxt = curtxt.rstrip("\n") + "\n" + newData
    text_display.delete(0.0, END)
    text_display.insert(END, newtxt)

def onUISaveCustomData(choice):
    global choices, text_display
    #meta = "<Meta>,{}".format(text_display.get(0.0, END)[:-1].replace("\n", ","))
    meta = text_display.get(0.0, END)[:-1].replace("\n", ",")
    for i, c in enumerate(choices):
        if c[0] == choice[0]:
            choices[i] = (c[0], c[1], c[2], c[3], c[4], meta)
            
    # only save in memory - don't write to file yet
    #writeUIToFile(filename)

def onUICloseExtraConfig(choice):
    onUISaveCustomData(choice)
    tl_extCnf.destroy()

def createExtraConfig(choice):
    global choices, tl_extCnf, chosenOption, text_display
    if "tl_extCnf" in globals() and tl_extCnf.winfo_exists():
        tl_extCnf.destroy()
    tl_extCnf = Toplevel(window)
    tl_extCnf.title("Choice customisation")
    
    # save choice when customisation closed
    tl_extCnf.protocol("WM_DELETE_WINDOW", lambda: onUICloseExtraConfig(choice))

    frame_newDataType = Frame(tl_extCnf)
    button_newDataType = Button(frame_newDataType, text="+", command=onUINewDataType)
    chosenOption = StringVar()
    opts_temp = ["Save data", "User input", "Conditional", "Command", "Note", "Load Adventure"]
    opmenu_newDataType = OptionMenu(frame_newDataType, chosenOption, *opts_temp)

    text_display = Text(tl_extCnf, width=40, height=6)
    text_display.insert(END, choice[5].replace(",", "\n").replace("/n", "\n"))

    # manual save button - doesn't save to file!
    button_saveData = Button(tl_extCnf, text="Save", command=lambda: onUISaveCustomData(choice))

    # binds hotkeys when focus is in any child of the new Toplevel
    bindGlobalHotkeys(tl_extCnf)

    # pack from left
    for w in [opmenu_newDataType, button_newDataType]:
        w.pack(side=LEFT, padx=5, pady=5)

    # pack from top
    for w in [frame_newDataType, text_display, button_saveData]:
        w.pack(padx=10, pady=5, fill=X, expand=TRUE)

def main():

    # create application window
    global window
    window = Tk()
    window.title("Text Based Adventure Creation Utility")

    # bind hotkeys when focus is in any child of window
    bindGlobalHotkeys(window)

    # initialise engine data
    init()

    # start tkinter mainloop
    window.mainloop()

if __name__ == '__main__':
    main()


"""Contains core functionality for the text adventure system
that is shared between the editor utility and standalone
player"""

# imports

from os import path
from tkinter import *
import winsound

# globals

adventuresdir = "MyAdventures" # folder to store adventures in
imagedirsuffix = "_Images" # relative directory to store images, eg MyAdventure_Images/... .png
musicdirsuffix = "_Music" # relative directory to store images, eg MyAdventure_Images/... .wav


 # # # # # # # # # # # # # # # # # # # # # # # #
 # save file utilities                         #
 # # # # # # # # # # # # # # # # # # # # # # # #

def writeDataToFile(filename, toSave):
    """Write the key-value dictionary data to the filename"""
    for key in toSave:
        lineNum = -1
        newLine = "{} = {}\n".format(key, toSave[key])
        # check if key in file already
        if path.exists(filename):
            with open(filename, "r") as file:
                for i, line in enumerate(file):
                    if line.split(" = ")[0] == key:
                        lineNum = i
                        break
            # write or update data to file
            if lineNum != -1:
                with open(filename, "r") as file:
                    newContents = file.readlines()
                    newContents[lineNum] = newLine
                with open(filename, "w") as file:
                    file.writelines(newContents)
                    return

        with open(filename, "a") as file:
            file.write(newLine)

def loadAllDataFromFile(filename):
    """Returns all saved data values from save file, in a dictionary"""
    data = {}
    if path.exists(filename):
        with open(filename, "r") as file:
            for line in file:
                try:
                    key, val = line[:-1].split(" = ")
                    data[key] = val
                except:
                    pass
    return data

def loadFromFile(filename, ID):
    """Returns a list of data connected with a page ID, if possible"""
    ID = str(ID)
    if not path.exists(filename): return
    with open(filename, "r") as file:
        for line in file:
            if line.split(",")[0] == ID:
                return line[:-1].split(",")
    return ","

 # # # # # # # # # # # # # # # # # # # # # # # #
 # page and choice tag extraction              #
 # # # # # # # # # # # # # # # # # # # # # # # #

def getMetaTags(filename, ID, choiceID):
    """Returns a list of meta tags connected to choiceID on page ID"""
    if not path.exists(filename):
        return
    parts = loadFromFile(filename, ID)
    metaTags = []
    if parts[0] == ID: #is this the line we want?
        i = 0
        testChoiceID = -1
        readingMeta = False
        for part in parts[2:]:
            if part == "<Choice>":
                testChoiceID += 1
                i = 1
                readingMeta = False
            # have we reached the right choice ID?
            elif testChoiceID == choiceID:
                if part == "<Meta>":
                    readingMeta = True
                elif readingMeta:
                    # add this meta tag to the meta tags string
                    metaTags.append(part)
    return metaTags

def breakMetaTag(tag):
    """Turns <command>(data) to ['command', 'data']"""
    try:
        command = tag.split(">(")[0][1:]
        data = tag.split(">(")[1][:-1]
        return [command, data]
    except:
        return []

def getImageForPage(filename, ID):
    """Finds the image to use for the corresponding page ID
       Returns None if not found.
       filename is the filename for the text adventure (*.txt)
       ID is the page ID to find image for"""
       
    image_id = str(ID) # use page ID by default

    # see if image id has been overridden for this page
    items = loadFromFile(filename, ID)
    for i, item in enumerate(items):
        if item == "<image>":
            image_id = items[i+1] # set image_id to set value

    # convert filename to image name
    # first make generic image name (no file extension)
    imageDir = filename[:-4] + imagedirsuffix + "/" + image_id

    # see if png file exists
    if path.exists(imageDir + ".png"):
        imageDir += ".png"
    # see if gif image exists (legacy)
    elif path.exists(imageDir + ".gif"):
        imageDir += ".gif"
    # otherwise return nothing :(
    else:
        return

    # create and return PhotoImage object
    return PhotoImage(file=imageDir)

def getMusicForPage(filename, ID):
    """Finds the music filename to use for the corresponding page ID
       
       Returns file path of music if music is found
       Returns 'STOP_ALL_MUSIC' if music id is 'STOP_ALL_MUSIC'
       Returns None if not found

       filename is the filename for the text adventure (*.txt)
       ID is the page ID to find image for"""
       
    music_id = str(ID) # use page ID by default

    # see if music id has been overridden for this page
    items = loadFromFile(filename, ID)
    for i, item in enumerate(items):
        if item == "<music>":
            music_id = items[i+1] # set music_id to set value

    # check if we want to stop music
    if music_id == 'STOP_ALL_MUSIC':
        return 'STOP_ALL_MUSIC'

    # convert filename to music name
    # first make generic music name (no file extension)
    musicDir = filename[:-4] + musicdirsuffix + "/" + music_id

    # see if wav file exists
    # winsound only supports wav
    if path.exists(musicDir + ".wav"):
        musicDir += ".wav"
    # otherwise return nothing :(
    else:
        return

    # create and return Photomusic object
    return musicDir

def playMusicForPage(filename, ID):
    """Plays music for a certain page on loop.
    
    Will override current music, if any.
    Will not stop current music if music for this page not found.
    Will stop current music if music for this page is 'STOP_ALL_MUSIC'"""

    # find the music for this page, if any

    music_file = getMusicForPage(filename, ID)

    # check if a file was returned
    if music_file:

        # stop all music if specified
        if music_file == "STOP_ALL_MUSIC":
            stopAllMusic()
            return

        winsound.PlaySound(music_file,
                   winsound.SND_FILENAME # treat music_file as a file
                   | winsound.SND_ASYNC  # don't interupt tkinter loop
                   | winsound.SND_LOOP)  # loop the music

def stopAllMusic():
    """Stops currently playing music, if any"""

    winsound.PlaySound(None, winsound.SND_FILENAME | winsound.SND_ASYNC)

if __name__ == "__main__":
    # for testing if correct image is being used
    window=Tk()
    img = getImageForPage("testAdventures/a.txt", 0)
    Label(window, image=img).pack()

    # for testing if correct music is being used
    music_file = getMusicForPage("testAdventures/a.txt", 0)
    print(music_file)
    if music_file:
        import winsound
        winsound.PlaySound(music_file, winsound.SND_FILENAME
                       | winsound.SND_ASYNC
                       | winsound.SND_LOOP)

    window.mainloop()
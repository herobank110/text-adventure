# text adventure

A text based adventure creation utility and standalone playing application.

## Wiki

Helpful instructional guide to get started:
[Wiki home](https://gitlab.com/herobank110/text-adventure/wikis)